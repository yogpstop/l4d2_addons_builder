#!/bin/bash
MYSQL_MAJVER="5.6"
MYSQL_VER="${MYSQL_MAJVER}.17"
GLIBC_VER="2.5"
ARCH="i686"
MYSQL="http://cdn.mysql.com/Downloads/MySQL-${MYSQL_MAJVER}/mysql-${MYSQL_VER}-linux-glibc${GLIBC_VER}-${ARCH}.tar.gz"
set -e
export LANG=en_US.UTF-8
amb() {
	mkdir tmp
	cd tmp
	python ../$1/configure.py $2
	if [ -d ".ambuild" ] ; then
		python ./build.py
	elif [ -d ".ambuild2" ] ; then
		ambuild
	else
		exit 1
	fi
	cp -ir package ..
	cd ..
	rm -rf tmp
}
if [ -z "${CONFOGL}" ] ; then
 CONFOGL=false
fi
rm -rf ambuild hl2sdk-l4d2 mmsource-central sourcemod-central left4downtown2 \
	l4dtoolz confoglcompmod tmp package mysql* strippersource \
	dhooks cannounce* l4d2_direct plugins Geo* builtinvotes \
	l4d2util L4D2-Plugins smplugins misc-sourcemod-plugins \
	come* player* customvotes* ProMod* l4d2readyup
hg clone https://hg.alliedmods.net/ambuild
hg clone https://hg.alliedmods.net/hl2sdks/hl2sdk-l4d2
hg clone https://hg.alliedmods.net/mmsource-central
hg clone https://hg.alliedmods.net/sourcemod-central
git clone git://github.com/ivailosp/l4dtoolz
wget -O come.zip "http://nico-op.forjp.net/lib/exe/fetch.php?media=l4d_come002.zip"
wget -O player.zip "http://nico-op.forjp.net/lib/exe/fetch.php?media=l4d_player007a.zip"
wget -nv -O cannounce.zip "https://forums.alliedmods.net/attachment.php?attachmentid=112140&d=1352851168"
wget -nv -O GeoIPCity-src.zip "https://forums.alliedmods.net/attachment.php?attachmentid=122082&d=1372844600"
wget -nv -O GeoIPCity.zip "https://forums.alliedmods.net/attachment.php?attachmentid=122081&d=1372844596"
wget -nv -O customvotes.zip "https://forums.alliedmods.net/attachment.php?attachmentid=35654&d=1231370687"
wget -nv -O mysql.tar.gz "${MYSQL}"
wget -nv http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
tar xvf mysql.tar.gz
mv mysql-* mysql-${MYSQL_MAJVER}
if ${CONFOGL} ; then
	hg clone https://code.google.com/p/left4downtown2
	git clone git://github.com/epilimic/confoglcompmod
	hg clone https://hg.alliedmods.net/strippersource
	hg clone https://bitbucket.org/Drifter321/dhooks
	git clone git://github.com/ConfoglTeam/l4d2_direct
	git clone git://github.com/epilimic/plugins
	svn co http://sourcemod-builtinvotes.googlecode.com/svn/trunk builtinvotes
	git clone git://github.com/ConfoglTeam/l4d2util
	git clone git://github.com/Tabbernaut/L4D2-Plugins
	git clone git://github.com/Attano/smplugins
	hg clone https://bitbucket.org/ProdigySim/misc-sourcemod-plugins
	git clone git://github.com/CanadaRox/l4d2readyup
	wget -nv -O ProMod.zip "http://l4dpromod.com/files/ver/ProMod3.7.zip"
fi

cd ambuild
sudo python setup.py install
cd ..
amb mmsource-central "-s l4d2"
amb sourcemod-central "-s l4d2"
cd l4dtoolz
patch -p1 -i ../l4dtz.patch
cp -i ../l4dtoolz.vdf .
make
cp -ir Release*/package/* ../package/addons/
cd ..
mkdir player
cd player
7z x ../player.zip
cp -fr "for sm-snapshot/"* ../package/
rm -r addons/sourcemod/plugins
mkdir addons/sourcemod/scripting
mv l4d_player.sp addons/sourcemod/scripting/
cp -ir addons ../package/
cd ..
mkdir customvotes
cd customvotes
7z x ../customvotes.zip
cp -ir addons/sourcemod/{configs,translations,scripting} ../package/addons/sourcemod/
cd ..
mkdir GeoIPCity GeoIPCity-src
cd GeoIPCity-src
7z x ../GeoIPCity-src.zip
patch -p1 -i ../gipc.patch
make
cp -i Release/*.so ../package/addons/sourcemod/extensions/
cd ..
cd GeoIPCity
7z x ../GeoIPCity.zip
cd ..
gzip -d GeoLiteCity.dat.gz
mv GeoLiteCity.dat package/addons/sourcemod/configs/geoip/GeoIPCity.dat
if ${CONFOGL} ; then
	cd left4downtown2
	patch -p1 -i ../l4dt2.patch
	make -f MakefileCI
	cp -ir Release*/package/* ../package/addons/sourcemod/
	cd ..
	cd socket
	make clean
	make
	cp -ir Release*/package/* ../package/addons/sourcemod/
	make clean
	cd ..
	cd strippersource
	rm -rf pcre
	svn co svn://vcs.exim.org/pcre/code/trunk pcre
	cd pcre
	./autogen.sh
	CFLAGS=-m32 CPPFLAGS=-m32 CXXFLAGS=-m32 LDFLAGS=-m32 PKG_CONFIG_PATH=/usr/lib/pkgconfig ./configure
	make
	cd ..
	patch -p1 -i ../ss.patch
	cd ..
	amb strippersource
	cd dhooks
	patch -p1 -i ../dhooks.patch
	make
	cp -i Release/*.so ../package/addons/sourcemod/extensions/
	cd ..
	cd l4d2_direct
	cp -ir gamedata scripting ../package/addons/sourcemod/
	cd ..
	cd builtinvotes
	patch -p0 -i ../biv.patch
	cd extension
	make
	cp -i Release/*.so ../../package/addons/sourcemod/extensions/
	cd ../..
fi
cd package/addons/sourcemod/scripting
mkdir -p ../plugins/optional
wget -nv -O l4d_players.sp "https://forums.alliedmods.net/attachment.php?attachmentid=65668&d=1273856508"
wget -nv -O include/colors.inc "https://forums.alliedmods.net/attachment.php?attachmentid=79000&d=1292171445"
7z x ../../../../come.zip l4d_come.sp
cp -ir ../../../../GeoIPCity/scripting/include .
if ${CONFOGL} ; then
	cp -ir ../../../../dhooks/sourcemod/scripting/include .
	cp -ir ../../../../confoglcompmod/include .
	cp -ir ../../../../l4d2util/scripting/include .
	cp -ir ../../../../L4D2-Plugins/penalty_bonus/include .
	cp -ir ../../../../smplugins/spechud/include .
	cp -i ../../../../misc-sourcemod-plugins/l4d2lib/l4d2lib.inc include/
	cp -ir ../../../../builtinvotes/plugin/addons/sourcemod/scripting/* .
	cp -ir ../../../../l4d2readyup/include .
	./spcomp builtinvotes_revote.sp -o../plugins/optional/builtinvotes_revote.smx
fi
./spcomp customvotes.sp -o../plugins/customvotes.smx
./spcomp l4d_come.sp -o../plugins/l4d_come.smx
./spcomp l4d_player.sp -o../plugins/l4d_player.smx
./spcomp l4d_players.sp -o../plugins/optional/l4d_players.smx
cd ../../../..
mkdir cannounce
cd cannounce
7z x ../cannounce.zip
cp -ir translations ../package/addons/sourcemod/
cd source
../../package/addons/sourcemod/scripting/spcomp cannounce.sp \
	-o../../package/addons/sourcemod/plugins/optional/cannounce.smx
cd ../..
if ${CONFOGL} ; then
	cd plugins/match_vote
	../../package/addons/sourcemod/scripting/spcomp match_vote.sp \
		-o../../package/addons/sourcemod/plugins/match_vote.smx
	cd ../panel_text
	../../package/addons/sourcemod/scripting/spcomp panel_text.sp \
		-o../../package/addons/sourcemod/plugins/optional/panel_text.smx
	cd ../..
	cd confoglcompmod
	../package/addons/sourcemod/scripting/spcomp confoglcompmod.sp \
		-o../package/addons/sourcemod/plugins/confoglcompmod.smx
	cd ..
	cd misc-sourcemod-plugins/l4d2lib
	../../package/addons/sourcemod/scripting/spcomp l4d2lib.sp \
		-o../../package/addons/sourcemod/plugins/optional/l4d2lib.smx
	cd ../..
	cd l4d2util/scripting
	../../package/addons/sourcemod/scripting/spcomp l4d2util.sp \
		-o../../package/addons/sourcemod/plugins/optional/l4d2util.smx
	cd ../..
	cd L4D2-Plugins/penalty_bonus
	../../package/addons/sourcemod/scripting/spcomp l4d2_penalty_bonus.sp \
		-o../../package/addons/sourcemod/plugins/optional/l4d2_penalty_bonus.smx
	cd ../..
	cd smplugins/spechud
	../../package/addons/sourcemod/scripting/spcomp spechud.sp \
		-o../../package/addons/sourcemod/plugins/optional/spechud.smx
	cd ../..
	cd l4d2readyup
	../package/addons/sourcemod/scripting/spcomp pause.sp \
		-o../package/addons/sourcemod/plugins/optional/pause.smx
	../package/addons/sourcemod/scripting/spcomp readyup.sp \
		-o../package/addons/sourcemod/plugins/optional/readyup.smx
	../package/addons/sourcemod/scripting/spcomp playermanagement.sp \
		-o../package/addons/sourcemod/plugins/optional/playermanagement.smx
	../package/addons/sourcemod/scripting/spcomp blocktrolls.sp \
		-o../package/addons/sourcemod/plugins/optional/blocktrolls.smx
	cd ..
	mkdir ProMod
	cd ProMod
	7z x ../ProMod.zip
	cp -ir "Fresh Install/"* ../package/
	cd ..
fi
rm -rf ambuild hl2sdk-l4d2 mmsource-central sourcemod-central left4downtown2 \
	l4dtoolz confoglcompmod tmp mysql* strippersource dhooks \
	cannounce* l4d2_direct plugins Geo* builtinvotes \
	l4d2util L4D2-Plugins smplugins misc-sourcemod-plugins \
	come* player* customvotes* ProMod* l4d2readyup
exit 0

http://dev.mysql.com/downloads/mysql/
