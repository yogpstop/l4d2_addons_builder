#!/bin/bash
#set -euxv
ROOTDIR="/root/Steam/SteamApps/common/Left 4 Dead 2 Dedicated Server"

L4D2DIR="${ROOTDIR}/left4dead2"
PLUGINDIR="${L4D2DIR}/addons/sourcemod/plugins"
BASEDIR="`dirname \`readlink -mqsn "$0"\``"
rm -rf "${L4D2DIR}/cfg/"{sourcemod,cfgogl,confogl*,server.cfg}
rm -rf "${L4D2DIR}/scripts/vscripts"
cd "${L4D2DIR}/addons"
rm -rf `ls -A1 | grep -v "readme.txt"`
cd "${BASEDIR}"
cat base.cfg >"${L4D2DIR}/cfg/server.cfg"
while [[ $# != 0 ]] ; do
	case "$1" in
		'help')
			echo "$0 [ mybase | dd2n ] [ cannounce ] [ si ] [ players.... ]"
			exit 0
			;;
		'cannounce')
			mv "${PLUGINDIR}/optional/cannounce.smx" "${PLUGINDIR}/"
			;;
		'mybase')
			cp -ir ../package/* "${L4D2DIR}/"
			;;
		'dd2n')
			7z x dd2n.zip -o"${ROOTDIR}" -x"!1.sh" -x"!left4dead2/cfg/server*.cfg"
			cp ../package/addons/sourcemod/plugins/l4d_{come,player}.smx "${PLUGINDIR}/"
			cat cfgl.cfg >>"${L4D2DIR}/cfg/server.cfg"
			;;
		'si')
			cat si.cfg >>"${L4D2DIR}/cfg/server.cfg"
			;;
		'players'[\ 1][0-9][\ 1][0-9])
			mv "${PLUGINDIR}/optional/l4d_players.smx" "${PLUGINDIR}/"
			cat players.cfg | sed -e "s/SL/${1: -4:2}/g" | sed -e "s/IL/${1: -2:2}/g" >>"${L4D2DIR}/cfg/server.cfg"
			;;
	esac
	shift
done
chmod +x "${L4D2DIR}/cfg/server.cfg"
exit 0
